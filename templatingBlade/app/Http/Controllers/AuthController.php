<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('register');
    }
    public function welcome(Request $request){
        $name = $request['name'];
        $fullname = $request['fullname'];
        return view ('welcome', compact('name','fullname'));
    }
    public function master(){
        return view('master');
    }

}
