	@extends('master')
	@section('judul')
	Halaman Edit Cast berid {{$cast->id}}
	
	@endsection

	@section('isi')

	<form action="/cast/{{$cast->id}}" method="POST">
	            @csrf
	            @method('put')
	            <div class="form-group">
	                <label for="title">Nama Cast</label>
	                <input type="text" class="form-control" name="nama" value="{{$cast->nama}}" id="title" placeholder="Masukkan Cast Name">
	                @error('nama')
	                    <div class="alert alert-danger">
	                        {{ $message }}
	                    </div>
	                @enderror
	            </div>
		        <div class="form-group">
	                <label for="umur">Umur</label>
	                <input type="text" class="form-control" name="umur" value="{{$cast->umur}}"id="umur" placeholder="Masukkan Umur">
	                @error('umur')
	                    <div class="alert alert-danger">
	                        {{ $message }}
	                    </div>
	                @enderror
	            </div>
	            <div class="form-group">
	                <label for="biodata">Biodata</label>
	                <input type="text" class="form-control" name="biodata" value="{{$cast->biodata}}" id="biodata" placeholder="Masukkan Segala Sesuatu Tentang Kamu">
	                @error('biodata')
	                    <div class="alert alert-danger">
	                        {{ $message }}
	                    </div>
	                @enderror
	            </div>
		            <button type="submit" class="btn btn-primary">Edit</button>
	        </form>
	@endsection